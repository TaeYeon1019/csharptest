1. "d"是*[标准日期和时间格式字符串](https://docs.microsoft.com/zh-cn/dotnet/standard/base-types/standard-date-and-time-format-strings#the-short-date-d-format-specifier)*，表示短日期格式；

2. "C2"是[标准数值格式字符串](https://docs.microsoft.com/zh-cn/dotnet/standard/base-types/standard-numeric-format-strings#currency-format-specifier-c)，用数字表示货币值;

   [^精确到小数点后两位]: 

   # Object-Oriented Programming(C#)

- #### four basic principles:

  - [ ] *Abstraction*

    [^[æbˈstrækʃn\] n .抽象]: 

  - [ ] *Encapsulation*

    [^封装]: 

  - [ ] *Inheritance*

  - [ ] *Polymorphism* 

    [^[ˌpɑˌliˈmɔrfɪzm\] 多态性]: 

    

